#include "Infection.h"

void Infection::IBacteria::ReflectType(Schematyc::CTypeDesc<IBacteria>& d)
{
	d.SetGUID("{D88B723F-D931-4089-9455-F17B7F13D70D}"_cry_guid);
	d.SetLabel("Bacteria");
	d.AddMember(&IBacteria::m_bIsGramPositive, 'gpos', "GPos", "Gram Positive", "Is the bacterium gram positive",
		false);
	d.AddMember(&IBacteria::m_bIsMotile, 'moti', "Motile", "Motile", "Is the bacterium motile",
		false);
	d.AddMember(&IBacteria::m_resistantClass, 'rcla', "ResistantClass", "Reisistant class", "Class of meds that aren't effective against the bacterium",
		Medication::EAntibioticClass::NONE);
	d.AddMember(&IBacteria::m_effectiveClass, 'effc', "EffectiveClass", "Effective class", "Class of meds that are effective against the bacterium", 
		Medication::EAntibioticClass::NONE);
}

void Medication::IIngredient::ReflectType(Schematyc::CTypeDesc<IIngredient>& d)
{
	d.SetGUID("{2AAB9FD9-330F-498A-86D8-7CECD7513AE7}"_cry_guid);
	d.SetLabel("Ingredient");
	d.AddMember(&IIngredient::m_name, 'name', "Name", "Name", "Name", "Ingredient");
	d.AddMember(&IIngredient::m_price, 'pric', "Price", "Price", "Price", 100.0f);
}

void Medication::IAntibiotic::ReflectType(Schematyc::CTypeDesc<IAntibiotic>& d)
{
	d.SetGUID("{176075DF-BB12-4FBC-A34E-EB73E8714B43}"_cry_guid);
	d.SetLabel("Antibiotic");
	d.AddMember(&IAntibiotic::m_name, 'name', "Name", "Name", "Name", "Name");
	d.AddMember(&IAntibiotic::m_weaknessTarget, 'weak', "Weakness", "Weakness", "Weaknesses of the bacterium",
		Medication::EBacterialWeaknesses::NONE);
	d.AddMember(&IAntibiotic::m_timespan, 'tspa', "Timespan", "Timespan", "How long it takes the antibiotic to work",
		Medication::EEffectiveTimeSpan::INSTANT);
	d.AddMember(&IAntibiotic::m_class, 'clas', "Class", "Class", "The class of the antibiotic",
		Medication::EAntibioticClass::NONE);
}

void Infection::IPathogen::ReflectType(Schematyc::CTypeDesc<IPathogen>& d)
{
	d.SetGUID("{FEA21E86-759D-4A28-A521-9F2A81E274B1}"_cry_guid);
	d.AddMember(&IPathogen::m_name, 'name', "Name", "Name", "Name of the pathogen", "");
	d.AddMember(&IPathogen::m_pathogenType, 'path', "PathType", "Pathogen Type", "The type of pathogen", Infection::EPathogenType::BACTERIAL);
}

void Infection::IVirus::ReflectType(Schematyc::CTypeDesc<IVirus>& d)
{
	d.SetGUID("{9FAAA50B-FD03-45A9-A6EA-EAF74F088912}"_cry_guid);
	d.SetLabel("Virus");
	d.SetDescription("A Virus");
	d.AddMember(&IVirus::m_genomeType, 'geno', "GenomeType", "Genome Type", "The Genome type of the Virus", EViralGenomeType::DNA_DOUBLE_STRANDED);
	d.AddMember(&IVirus::m_bIsBudding, 'budd', "IsBudding", "Is Budding", "If the new virus particles bud from the cell", true);
}
