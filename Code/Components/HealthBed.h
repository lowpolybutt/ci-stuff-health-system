#pragma once

#include "StdAfx.h"

#include "ActorStatus.h"

#include <CryCore/StaticInstanceList.h>
#include <CryEntitySystem/IEntityComponent.h>
#include <CryGame/IGameFramework.h>
#include <CrySchematyc/CoreApi.h>

class CHealthBedComponent final
	: public IEntityComponent
{
public:
	static void ReflectType(Schematyc::CTypeDesc<CHealthBedComponent>& d);

	void OnActorEntered(IStatusActor* a);
	// Performs the immunoassay
	void GetDetailedInfection(Infection::IPathogen p);

private:
	// Contains the basic health info to display on the "screen"
	struct SHealthDisplayInfo
	{
		string m_genericText;
		bool m_bIsInfected;
	};

	// Outputs Health info to the "screen"
	void DisplayHealthInfo(SHealthDisplayInfo i);

	// Converts the raw ActorStatus into displayable info
	void ParseActorStatus(IStatusActor::SActorStatus s);

	// Holds a pointer to any actor entering the bed
	IStatusActor* m_pActor;

	void ParseBacterialInfection(Infection::IBacteria b);
	void ParseViralInfection(Infection::IVirus v);
};