#include "ActorStatus.h"

void IStatusActor::ReflectType(Schematyc::CTypeDesc<IStatusActor>& d)
{
	d.SetGUID("{08163512-E151-4F65-99A0-3BB81A830652}"_cry_guid);
}

void IStatusActor::SActorStatus::ReflectType(Schematyc::CTypeDesc<IStatusActor>& d)
{
	d.SetGUID("{96EE1731-5DDC-4694-B871-713AF45EB1CF}"_cry_guid);
}

void IStatusActor::ReflectType(Schematyc::CTypeDesc<IStatusActor::EInfectionStatus>& d)
{
	d.SetGUID("{F38F991B-FDE4-48A5-BF0C-448A095D179D}"_cry_guid);
}