#include "Room.h"

namespace
{
	void RegisterRoomComponent(Schematyc::IEnvRegistrar& r)
	{
		Schematyc::CEnvRegistrationScope c = r.Scope(IEntity::GetEntityScopeGUID());
		{
			Schematyc::CEnvRegistrationScope sC = c.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRoomComponent));
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterRoomComponent);
}

void CRoomComponent::ReflectType(Schematyc::CTypeDesc<CRoomComponent>& d)
{
	d.SetGUID("{BD9EB4FF-3E2C-48E2-A324-512ED27640DF}"_cry_guid);
	d.SetLabel("Room");
	d.SetEditorCategory("Health");
	d.AddMember(&CRoomComponent::m_bacteria, 'bact', "Bacteria", "Bacteria", "The airborne bacteria in the room",
		Infection::IBacteria());
	d.AddMember(&CRoomComponent::m_virus, 'viru', "Virus", "Virus", "The airborne virus in the room", Infection::IVirus());
}

void CRoomComponent::Initialize()
{
	IEntityTriggerComponent* pTriggerComponent = m_pEntity->CreateComponent<IEntityTriggerComponent>();
	const Vec3 box = Vec3(2, 2, 2);
	const AABB bounds = AABB(box * -.5f, box * .5f);
	pTriggerComponent->SetTriggerBounds(bounds);
}

Cry::Entity::EventFlags CRoomComponent::GetEventMask() const
{
	return EEntityEvent::EntityEnteredThisArea;
}

void CRoomComponent::ProcessEvent(const SEntityEvent& s)
{
	if (s.event == EEntityEvent::EntityEnteredThisArea)
	{
		const EntityId eEntity = static_cast<EntityId>(s.nParam[0]);
		IEntity* e = gEnv->pEntitySystem->GetEntity(eEntity);
		if (CPlayerComponent* c = e->GetComponent<CPlayerComponent>())
		{
			if (Infection::IBacteria() != m_bacteria)
			{
				c->SetBacterialInfection(m_bacteria);
			}

			if (Infection::IVirus() != m_virus)
			{
				c->SetViralInfection(m_virus);
			}
		}
	}
}
